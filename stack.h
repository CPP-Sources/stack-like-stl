/*
    
    /-------\  /-------\  /       \  /-------\  /-------\  /-------\  /
    |          |       |  |       |  |              |      /       \  |
    |          |       |  |       |  |              |      /       \  |
    |          \-------/  \-------/  \-------\      |      /-------\  |
    |          |  \               |          |      |      /       \  |
    |          |     \            |          |      |      /       \  |
    \-------/  |       \  \-------/  \-------/      |      /       \  \-------/
    
    /-------\  /-------\  /       \  |-----\    ---------  /-------\
    |              |      |       |  |      \       |      |       |
    |              |      |       |  |       \      |      |       |
    \-------\      |      |       |  |       |      |      |       |
            |      |      |       |  |       /      |      |       |
            |      |      |       |  |      /       |      |       |
    \-------/      |      \-------/  |-----/    ---------  \-------/
        
*/

/*
 *   This file contains descriptions of
 *   dynamical stack structure
 *   
 *   28 June 2018
 *   -isst
*/

#ifndef STACK_H
#define STACK_H

template <class T> //template for a *NODE* struct
struct node
{
	T value; //the value of type T
	node<T>* next; //pointer o the next node
	node(){}
	node(T val) {value = val; next = 0;} //constructor with value only
	node(T val, node<T>* nxt) {value = (next = nxt, val);} //constructor with both parameters
};

typedef int size_type;

template <class T> //template for a *STACK* class
class stack
{
private:
	node<T>* head; //head of a stack	
public:
	//constructors
	stack() {head = 0;} //default constructor
	stack(stack&); //copy constructor
	stack(node<T>); //constructor by node

	//destructors
	~stack(); //default destructor
	
	//operators
	void operator = (stack<T>&); //equation operator

	//element access
	bool top(T&); //access to the top element

	//capacity
	bool empty(); //checks if stack is empty
	size_type size(); //returns a size of stack

	//modifiers
	void push(const T); //pushes an element to a stack
	bool emplace(const T); //emplaces an element at the head of stack
	bool pop(); //removes an element
	void swap(stack&); //swaps two stacks
	bool clear(); //clears a stack

	template <class X> friend bool operator == (stack<X>, stack<X>);
	template <class X> friend bool operator != (stack<X>, stack<X>);
	template <class X> friend bool operator < (stack<X>, stack<X>);
	template <class X> friend bool operator <= (stack<X>, stack<X>);
	template <class X> friend bool operator > (stack<X>, stack<X>);
	template <class X> friend bool operator >= (stack<X>, stack<X>);
};

template <class T> stack<T>::stack(stack& other) //copy constructor
{
	head = 0; //default for empty stack

	stack<T> buffer;
	node<T>* tmp = other.head;
	while (tmp != 0)
	{
		node<T>* cur = new node<T>(tmp -> value, buffer.head);
		buffer.head = (tmp = tmp->next, cur);
	}

	tmp = buffer.head;
	while (tmp != 0)
	{
		node<T>* cur = new node<T>(tmp -> value, head);
		head = (tmp = tmp->next, cur);
	}
}

template <class T> stack<T>::stack(node<T> other) //constructor by node
{
	head = 0; //default for empty stack

	stack<T> buffer;
	node<T>* tmp = &other;
	while (tmp != 0)
	{
		node<T>* cur = new node<T>(tmp -> value, buffer.head);
		buffer.head = (tmp = tmp -> next, cur);
	}

	tmp = buffer.head;
	while (tmp != 0)
	{
		node<T>* cur = new node<T>(tmp->value, head);
		head = (tmp = tmp->next, cur);
	}
}

template <class T> stack<T>::~stack()
{
	clear(); //just clearing
}

template <class T> void stack<T>::operator = (stack<T>& other)
{
	clear();
	
	stack<T> buffer;
	node<T>* tmp = other.head;
	while (tmp != 0)
	{
		node<T>* cur = new node<T>(tmp->value, buffer.head);
		buffer.head = (tmp = tmp->next, cur);
	}

	tmp = buffer.head;
	while (tmp != 0)
	{
		node<T>* cur = new node<T>(tmp->value, head);
		head = (tmp = tmp->next, cur);
	}
}

template <class T> bool stack<T>::top(T& buf) //access to the top element
{
	return (head == 0 ? false : (buf = head -> value, true)); //returns top element and checks
}

template <class T> bool stack<T>::empty() //checks if stack is empty
{
	return (head == 0); //just checks header
}

template <class T> size_type stack<T>::size() //returns a size of stack
{
	size_type counter = 0; //var a counter
	node<T>* tmp = head; //setting a temporary node
	while (tmp != 0) 
		counter += (tmp = tmp -> next, 1); //counts and changes
	return counter; //returns a value
}

template <class T> void stack<T>::push(const T val) //pushes an element to a stack
{
	node<T>* cur = new node<T>(val, head); //creating a new node
	head = cur; //resets header
}

template <class T> bool stack<T>::emplace(const T val) //emplaces an element at the head of stack
{
	return (head == 0 ? false : (head -> value = val, true)); //emplaces and checks
}

template <class T> bool stack<T>::pop() //removes an element
{
	if (head == 0) //if header is empty
		return false; //returns false
	node<T>* tmp = head; //saving pointer for the head
	head = head -> next; //resets head
	delete tmp; //free memory
	return true; //returns true than
}

template <class T> void stack<T>::swap(stack& other) //swaps two stacks
{
	node<T>* buf = head;
	head = other.head;
	other.head = buf;
}

template <class T> bool stack<T>::clear()
{
	if (head == 0)
		return false;
	while (head != 0) //while elements exist
	{
		node<T>* tmp = head; //saving current pointer
		head = head -> next; //modifying header
		delete tmp; //free memory
	}
	head = 0;
	return true;
}

template <class X> bool operator == (stack<X> s, stack<X> s1)
{
	return (s.size() == s1.size());
}

template <class X> bool operator != (stack<X> s, stack<X> s1)
{
	return (s.size() != s1.size());
}

template <class X> bool operator < (stack<X> s, stack<X> s1)
{
	return (s.size() < s1.size());
}

template <class X> bool operator <= (stack<X> s, stack<X> s1)
{
	return (s.size() <= s1.size());
}

template <class X> bool operator > (stack<X> s, stack<X> s1)
{
	return (s.size() > s1.size());
}

template <class X> bool operator >= (stack<X> s, stack<X> s1)
{
	return (s.size() >= s1.size());
}

#endif