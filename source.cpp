/*
    
    /-------\  /-------\  /       \  /-------\  /-------\  /-------\  /
    |          |       |  |       |  |              |      /       \  |
    |          |       |  |       |  |              |      /       \  |
    |          \-------/  \-------/  \-------\      |      /-------\  |
    |          |  \               |          |      |      /       \  |
    |          |     \            |          |      |      /       \  |
    \-------/  |       \  \-------/  \-------/      |      /       \  \-------/
    
    /-------\  /-------\  /       \  |-----\    ---------  /-------\
    |              |      |       |  |      \       |      |       |
    |              |      |       |  |       \      |      |       |
    \-------\      |      |       |  |       |      |      |       |
            |      |      |       |  |       /      |      |       |
            |      |      |       |  |      /       |      |       |
    \-------/      |      \-------/  |-----/    ---------  \-------/
        
*/

/*
 *   This file contains some testing code
 *   for dynamical stack structure. You can
 *   watch vars using debugger, as you know ;)
 *   
 *   28 June 2018
 *   -isst
*/

#include "header.h" //including our header

int main()
{
	stack<int> s; //declaring a new stack, default constructor works
	
	s.push(4); //pushing a number
	s.push(3); //pushing another number
	s.push(23); //pushing another number
	s.push(13); //pushing another number
	s.push(53); //pushing another number

	bool b = s.empty();

	int x;
	s.top(x); //getting a top number

	s.pop(); //removing a heading element
	s.top(x); //getting a top element
	x = s.size(); //getting a size of stack
	s.emplace(444); //emplacing a top element

	s.clear(); //clearing a stack;
	b = s.empty(); //checking if s is empty
	x = s.size();
	s.emplace(1111); //emplacing a top element

	s.push(1);
	s.push(2);
	s.push(3);

	stack<int> s1(s); //copy constructor for other stack

	node<int> n;
	n.value = 1;
	n.next = new node<int>(2, 0);

	stack<int> s2(n); //copy constructor for node

	s1.swap(s2); //swaps s1 and s2

	s1 = s2; //equation operator works

	b = s1 > s2;

	return 0;
}